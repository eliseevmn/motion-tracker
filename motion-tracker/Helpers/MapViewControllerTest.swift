//import UIKit
//import GoogleMaps
//import CoreLocation
//
// Экран определения маршрута
//class MapViewController: UIViewController {
//
//    // MARK: - Outlets
//    private lazy var mapView: GMSMapView = {
//        let map = GMSMapView.init(frame: self.view.frame)
//        return map
//    }()
//
//    private let currentLocationButton: UIButton = {
//        let button = UIButton()
//        button.setImage(#imageLiteral(resourceName: "location"), for: .normal)
//        button.addTarget(self, action: #selector(handleCurrenLocation), for: .touchUpInside)
//        button.widthAnchor.constraint(equalToConstant: 40).isActive = true
//        button.heightAnchor.constraint(equalToConstant: 40).isActive = true
//        return button
//    }()
//
//    private let switchStartStopRouteButton: CustomButton = {
//        let button = CustomButton(title: "Начать новый трек")
//        button.addTarget(self, action: #selector(handleStartStropUpdateRoute), for: .touchUpInside)
//        return button
//    }()
//
//    private let previousRouteButton: CustomButton = {
//        let button = CustomButton(title: "Отобразить предыдущий маршрут")
//        button.addTarget(self, action: #selector(handlePreviousButton), for: .touchUpInside)
//        return button
//    }()
//
//    // MARK: - Properties
//    private let locationManager = CLLocationManager()
//    private var location: CLLocationCoordinate2D?
//    private var geocoder = CLGeocoder()
//
//    private var marker: GMSMarker?
//    private var route: GMSPolyline?
//    private var routePath: GMSMutablePath?
//    private var isRouteButtonStatus: Bool = true
//    private var isRoutePreviuosButtonStatus: Bool = false
//
//    // MARK: - ViewController lifecycles
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        configureMapAndUI()
//        configureNavBar()
//        getCurentLocation()
//        MapServiceStyle.configureMapStyle(mapView: mapView)
//        locationManager.stopUpdatingLocation()
//    }
//
//    // MARK: - ConfigureUI
//    private func configureMapAndUI() {
//        view.addSubview(mapView)
//        view.addSubview(currentLocationButton)
//        currentLocationButton.anchor(top: view.topAnchor,
//                                leading: nil,
//                                bottom: nil,
//                                trailing: view.trailingAnchor,
//                                padding: .init(top: 100, left: 0, bottom: 0, right: 40))
//
//        let buttonsStackView = UIStackView(arrangedSubviews: [
//            previousRouteButton, switchStartStopRouteButton
//        ])
//        view.addSubview(buttonsStackView)
//        buttonsStackView.anchor(top: nil,
//                                leading: view.leadingAnchor,
//                                bottom: view.bottomAnchor,
//                                trailing: view.trailingAnchor,
//                                padding: .init(top: 0, left: 20, bottom: 40, right: 20))
//        buttonsStackView.axis = .vertical
//        buttonsStackView.spacing = 10
//    }
//
//    private func configureNavBar() {
//        navigationItem.title = "Трекер перемещений"
//    }
//
//    // MARK: - Configure location
//    private func getCurentLocation() {
//        self.locationManager.requestAlwaysAuthorization()
//
//        if CLLocationManager.locationServicesEnabled() {
//            locationManager.delegate = self
//            locationManager.desiredAccuracy = kCLLocationAccuracyBest
//            locationManager.allowsBackgroundLocationUpdates = true
//            locationManager.pausesLocationUpdatesAutomatically = false
//            locationManager.startMonitoringSignificantLocationChanges()
//            locationManager.startUpdatingLocation()
//        }
//    }
//
//    @objc private func handleCurrenLocation() {
//        guard let location = location else { return }
//        configureCameraMap(coordinate: location)
//    }
//
//    private func configureCameraMap(coordinate: CLLocationCoordinate2D) {
//        let camera = GMSCameraPosition.camera(withLatitude: coordinate.latitude,
//                                              longitude: coordinate.longitude,
//                                              zoom: 15)
//        mapView.camera = camera
//    }
//
//    private func addMarker(coordinate: CLLocationCoordinate2D, placeMark: CLPlacemark) {
//        marker = GMSMarker(position: coordinate)
//        marker?.icon = GMSMarker.markerImage(with: .green)
//        marker?.title = placeMark.locality
//        marker?.snippet = placeMark.name
//        marker?.groundAnchor = CGPoint(x: 0.5, y: 1)
//        marker?.map = mapView
//    }
//
//    private func removeMarker() {
//        marker?.map = nil
//        marker = nil
//    }
//
//    // MARK: - Update Route
//    @objc private func handleStartStropUpdateRoute() {
//        print("handleStartStropUpdateRoute")
//        if isRouteButtonStatus {
//            startRouteTracking()
//            isRouteButtonStatus = false
//            isRoutePreviuosButtonStatus = true
//        } else {
//            stopRouteTracking()
//            isRouteButtonStatus = true
//            isRoutePreviuosButtonStatus = false
//        }
//    }
//
//    private func startRouteTracking() {
//        previousRouteButton.isEnabled = true
//        previousRouteButton.layer.opacity = 1
//
//        switchStartStopRouteButton.setTitle("Закончить трек", for: .normal)
//        route?.map = nil
//        route = RoutePolyline(routeWidth: 7, colorRoute: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
//        routePath = GMSMutablePath()
//        route?.map = mapView
//        locationManager.startUpdatingLocation()
//    }
//
//    private func stopRouteTracking() {
//        switchStartStopRouteButton.setTitle("Начать новый трек", for: .normal)
//        locationManager.stopUpdatingLocation()
//
//        guard let routePath = self.routePath else { return }
//        RealmService.saveRoute(route: routePath)
//    }
//
//    // MARK: - Load Previuos RoutePath
//    @objc private func handlePreviousButton() {
//        if isRoutePreviuosButtonStatus {
//            showStopTrackingAlert()
//            locationManager.stopUpdatingLocation()
//            isRoutePreviuosButtonStatus = false
//        } else {
//            loadPreviousRoute()
//
//            previousRouteButton.isEnabled = false
//            previousRouteButton.layer.opacity = 0.2
//        }
//    }
//
//    private func showStopTrackingAlert() {
//        let alertController = UIAlertController(title: "Отслеживание включено!",
//                                                message: "Сначало необходимо отключить отслеживание",
//                                                preferredStyle: .alert)
//        let confirmAction = UIAlertAction(title: "ОК", style: .destructive) { [weak self] _ in
//            self?.stopRouteTracking()
//            self?.loadPreviousRoute()
//        }
//        isRouteButtonStatus = true
//
//        alertController.addAction(confirmAction)
//        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { [weak self] _ in
//            self?.stopRouteTracking()
//        }
//        alertController.addAction(cancelAction)
//        present(alertController, animated: true, completion: nil)
//    }
//
//    private func loadPreviousRoute() {
//        route?.map = nil
//        route = RoutePolyline(routeWidth: 7, colorRoute: #colorLiteral(red: 0.9098039216, green: 0.2705882353, blue: 0.3529411765, alpha: 1))
//        routePath = RealmService.loadPreviuosRoute()
//        route?.path = routePath
//        route?.map = mapView
//
//        guard let setBounds = routePath else { return }
//        let bounds = GMSCoordinateBounds(path: setBounds)
//        mapView.animate(with: GMSCameraUpdate.fit(bounds))
//    }
//}
//
// MARK: - CLLocationManagerDelegate
//extension MapViewController: CLLocationManagerDelegate {
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        removeMarker()
//
//        guard let location = locations.last else { return }
//        self.location = locations.first?.coordinate
//        configureCameraMap(coordinate: location.coordinate)
//
//        geocoder.reverseGeocodeLocation(location) { (_, error) in
//            if let error = error {
//                print(error.localizedDescription)
//                return
//            }
//            if let place = places?.last {
//                self.addMarker(coordinate: location.coordinate, placeMark: place)
//            }
//        }
//        self.routePath?.add(location.coordinate)
//        self.route?.path = self.routePath
//        removeMarker()
//    }
//
//    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
//        print(error.localizedDescription)
//    }
//}
