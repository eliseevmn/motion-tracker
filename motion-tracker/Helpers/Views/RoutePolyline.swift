import Foundation
import GoogleMaps

class RoutePolyline: GMSPolyline {

    let routeWidth: CGFloat
    let colorRoute: UIColor

    init(routeWidth: CGFloat, colorRoute: UIColor) {
        self.routeWidth = routeWidth
        self.colorRoute = colorRoute
        super.init()

        self.strokeWidth = routeWidth
        self.strokeColor = colorRoute
    }
}
