import UIKit

extension UIImageView {

    convenience init(image: UIImage?, contentMode: UIView.ContentMode, width: CGFloat?, tintColor: UIColor?) {
        self.init()

        translatesAutoresizingMaskIntoConstraints = false
        widthAnchor.constraint(equalToConstant: width ?? 0).isActive = true
        self.image = image
        self.contentMode = contentMode
        self.tintColor = tintColor
    }
}

extension UIImageView {
    func setupColor(color: UIColor) {
        let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
}

extension UIImageView {

    func saveImageToDocumentDirectory(_ image: UIImage, name: String) {
        let fileManager = FileManager.default
        let paths = (
            NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(name)

        print(paths)

        let imageData = image.jpegData(compressionQuality: 0.5)
        fileManager.createFile(atPath: paths as String, contents: imageData, attributes: nil)
    }

    func getImage(name: String) -> UIImage? {
        let fileManager = FileManager.default
        let imagePAth = (self.getDirectoryPath() as NSString).appendingPathComponent(name)
        if fileManager.fileExists(atPath: imagePAth) {
            return UIImage(contentsOfFile: imagePAth)
        } else {
            print("No Image")
            return nil
        }

    }

    private func getDirectoryPath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
}
