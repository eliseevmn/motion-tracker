import UIKit

class AddPhotoView: UIView {

    // MARK: - Outlets
    var circleImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = #imageLiteral(resourceName: "avatar")
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.borderColor = UIColor.black.cgColor
        imageView.layer.borderWidth = 1
        return imageView
    }()

    let plusButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        let myImage = #imageLiteral(resourceName: "plus")
        button.setImage(myImage, for: .normal)
        button.tintColor = .darkGray
        return button
    }()

    // MARK: - View
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        circleImageView.layer.masksToBounds = true
        circleImageView.layer.cornerRadius = circleImageView.frame.width / 2
    }

    // MARK: - Setup UI
    private func setupConstraints() {

        self.addSubview(circleImageView)
        self.addSubview(plusButton)

        circleImageView.anchor(top: self.topAnchor,
                               leading: self.leadingAnchor,
                               bottom: nil,
                               trailing: nil,
                               size: .init(width: 100, height: 100))
        plusButton.anchor(top: nil,
                          leading: circleImageView.trailingAnchor,
                          bottom: nil, trailing: nil,
                          padding: .init(top: 0, left: 16, bottom: 0, right: 0),
                          size: .init(width: 30, height: 30))
        plusButton.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true

        self.bottomAnchor.constraint(equalTo: circleImageView.bottomAnchor).isActive = true
        self.trailingAnchor.constraint(equalTo: plusButton.trailingAnchor).isActive = true
    }
}
