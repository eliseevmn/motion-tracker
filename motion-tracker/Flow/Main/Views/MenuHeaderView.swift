import UIKit

class MenuHeaderView: UIView {

    // MARK: - Outlets
    let nameLabel = UILabel()

    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)

        backgroundColor = #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1)
        setupStackView()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Setup UI
    private func setupStackView() {

        nameLabel.font = UIFont.boldSystemFont(ofSize: 22)
        nameLabel.textColor = .white

        let arrangedSubviews = [
            nameLabel
        ]
        let stackView = UIStackView(arrangedSubviews: arrangedSubviews)
        stackView.axis = .vertical
        stackView.spacing = 4

        addSubview(stackView)
        stackView.fillSuperview()

        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.layoutMargins = .init(top: 24, left: 24, bottom: 24, right: 24)
    }
}
