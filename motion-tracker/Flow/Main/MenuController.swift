import UIKit

class MenuController: UIViewController {

    // MARK: - Outlets
    let tableView = UITableView(frame: .zero, style: .plain)
    let logoutButton = UIButton(title: "Logout", titleColor: .blue)

    // MARK: - Properties
    let menuItems = [
        [
            MenuItem(title: "HomeWork GeekBrains")
        ],

        [
            MenuItem(title: "FindPlaces MapKit"),
            MenuItem(title: "Places MapKit"),
            MenuItem(title: "Directions MapKit")
        ],

        [
            MenuItem(title: "FindPlaces GoogleMaps (в разработке)"),
            MenuItem(title: "Places GoogleMaps (в разработке)"),
            MenuItem(title: "Directions GoogleMaps (в разработке)")
        ]
    ]

    // MARK: - View lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupTableView()
    }

    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
    }

    // MARK: - Private functions
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
    }
}

// MARK: - UITableViewDataSource
extension MenuController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        let text = section == 0 ? "HW GeekBrains" : section == 1 ? "MapKit" : "GoogleMaps"
        let customHeader = MenuHeaderView()

        customHeader.nameLabel.text = text
        return customHeader
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return menuItems.count
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? "HW GeekBrains" : section == 1 ? "MapKit" : "GoogleMaps"
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems[section].count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = MenuItemCell(style: .default, reuseIdentifier: nil)

        let menuItem = menuItems[indexPath.section][indexPath.row]
        cell.titleLabel.text = menuItem.title

        return cell
    }
}

// MARK: - UITableViewDelegate
extension MenuController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.section {
        case 0:
            if indexPath.row == 0 {
                let controller = MapGBViewController()
                navigationController?.pushViewController(controller, animated: true)
            }
        case 1:
            switch indexPath.row {
            case 0:
                let controller = SearchPlaceController()
                controller.view.backgroundColor = .white
                navigationController?.pushViewController(controller, animated: true)
            case 1:
                let controller = PlacesMapKitViewController()
                controller.view.backgroundColor = .white
                navigationController?.pushViewController(controller, animated: true)
            case 2:
                let controller = DirectionsMapKitController()
                controller.view.backgroundColor = .white
                navigationController?.pushViewController(controller, animated: true)
            default:
                break
            }
        case 2:
            switch indexPath.row {
            case 0:
                let controller = UIViewController()
                controller.view.backgroundColor = .white
                navigationController?.pushViewController(controller, animated: true)
            case 1:
                let controller = SearchPlacesGoogleMapViewController()
                controller.view.backgroundColor = .white
                navigationController?.pushViewController(controller, animated: true)
            case 2:
                let controller = DirectionsGoogleMapsController()
                controller.view.backgroundColor = .white
                navigationController?.pushViewController(controller, animated: true)
            default:
                break
            }
        default:
            break
        }
    }
}

// MARK: - Setup UI
extension MenuController {
    private func setupUI() {
        view.addSubview(tableView)
        tableView.fillSuperview()

        navigationItem.title = "Список вариантов карт"
    }
}
