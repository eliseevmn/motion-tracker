import UIKit

final class MainViewController: UIViewController {

    // MARK: - Outlets
    private let toMapButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(showMap), for: .touchUpInside)
        button.setTitle("Показать карту", for: .normal)
        button.setTitleColor(.blue, for: .normal)
        return button
    }()

    private let exitButton: UIButton = {
        let button = UIButton()
        button.setTitle("Выйти", for: .normal)
        button.addTarget(self, action: #selector(logout), for: .touchUpInside)
        button.setTitleColor(.blue, for: .normal)
        return button
    }()

    private let addPhotoView = AddPhotoView()

    // MARK: - Properties
    var onMap: (() -> Void)?
    var onLogout: (() -> Void)?

    // MARK: - ViewController lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupUI()

        addPhotoView.plusButton.addTarget(self, action: #selector(addPhotoButtonTapped), for: .touchUpInside)
        addPhotoView.circleImageView.image = addPhotoView.circleImageView.getImage(name: "logo")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
}

// MARK: - Actions
extension MainViewController {
    @objc private func showMap(_ sender: Any) {
        onMap?()
    }

    @objc private func logout(_ sender: Any) {
        UserDefaults.standard.set(false, forKey: "isLogin")
        onLogout?()
    }

    @objc private func addPhotoButtonTapped() {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.sourceType = .photoLibrary
        present(imagePickerController, animated: true, completion: nil)
    }
}

// MARK: - UIImagePickerControllerDelegate
extension MainViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(
        _ picker: UIImagePickerController,
        didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
        addPhotoView.circleImageView.image = image
        addPhotoView.circleImageView.saveImageToDocumentDirectory(image, name: "logo")
    }
}

// MARK: - Setup UI
extension MainViewController {
    private func setupUI() {
        let stackView = UIStackView(arrangedSubviews: [
            addPhotoView,
            toMapButton,
            exitButton
        ])
        view.addSubview(stackView)
        stackView.axis = .vertical
        stackView.spacing = 20
        stackView.centerInSuperview()
    }
}
