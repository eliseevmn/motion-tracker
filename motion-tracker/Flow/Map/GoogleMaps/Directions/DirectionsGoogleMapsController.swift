import UIKit
import CoreLocation
import GoogleMaps
import GooglePlaces

class DirectionsGoogleMapsController: UIViewController {

    // MARK: - Outlets
    private lazy var mapView: GMSMapView = {
        let map = GMSMapView.init(frame: self.view.frame)
        return map
    }()
    private let navBar: UIView = {
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.2587935925, green: 0.5251715779, blue: 0.9613835216, alpha: 1)
        return view
    }()

    private let startTextField = DirectionsTextField(padding: 12,
                                                     cornerRadius: 5)
    private let endTextField = DirectionsTextField(padding: 12,
                                                   cornerRadius: 5)

    private let showRouteButton = UIButton(title: "Показать маршрут",
                                           titleColor: .black,
                                           backgroundColor: .white,
                                           font: .boldSystemFont(ofSize: 16),
                                           isShadow: true,
                                           cornerRadius: 5)

    private let startIcon = UIImageView(
        image: #imageLiteral(resourceName: "startlocatino").withRenderingMode(.alwaysTemplate),
        contentMode: .scaleAspectFit, width: 20, tintColor: .white)
    private let endIcon = UIImageView(
        image: #imageLiteral(resourceName: "endlocation").withRenderingMode(.alwaysTemplate),
        contentMode: .scaleAspectFit, width: 20, tintColor: .white)
    let activityController = UIActivityIndicatorView(style: .large)
    let backButton = UIButton(title: "Назад", titleColor: .black)

    // MARK: - Properties
    private let locationManager = CLLocationManager()

    // MARK: - View lyfecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        setupUI()
        setupShowRouteButton()

        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()

        showRouteButton.addTarget(self, action: #selector(handleShowRoute), for: .touchUpInside)
        backButton.addTarget(self, action: #selector(handleBack), for: .touchUpInside)
        startTextField.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleChangeStartLocation)))
        endTextField.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleCnahgeEndLocation)))
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }

    // MARK: Map functions
    private func setupMapViewSettings() {
        mapView.isMyLocationEnabled = true
        mapView.delegate = self
        mapView.settings.myLocationButton = true
        mapView.settings.compassButton = true
        mapView.settings.zoomGestures = true
    }

    func createMarker(titleMarker: String, iconMarker: UIImage, coordinate: CLLocationCoordinate2D) {
        let marker = GMSMarker()
        marker.position = coordinate
        marker.title = titleMarker
        marker.icon = iconMarker
        marker.map = mapView
    }

    func searchPlaceWithAutoComplete() {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self

        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
            UInt(GMSPlaceField.placeID.rawValue))!
        autocompleteController.placeFields = fields

        let filter = GMSAutocompleteFilter()
        filter.type = .address
        autocompleteController.autocompleteFilter = filter

        present(autocompleteController, animated: true, completion: nil)
    }
}

// MARK: - Actions
extension DirectionsGoogleMapsController {
    @objc private func handleBack() {
        self.navigationController?.popViewController(animated: true)
    }

    @objc private func handleShowRoute() {

    }

    @objc private func handleChangeStartLocation() {
        searchPlaceWithAutoComplete()
    }

    @objc private func handleCnahgeEndLocation() {
        searchPlaceWithAutoComplete()
    }
}

extension DirectionsGoogleMapsController: GMSAutocompleteViewControllerDelegate {

    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name ?? "")")
        print("Place ID: \(place.placeID ?? "")")
        print("Place attributions: \(String(describing: place.attributions))")
        dismiss(animated: true, completion: nil)
    }

    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }

    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
}

// MARK: - CLLocationManagerDelegate
extension DirectionsGoogleMapsController: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse || status == .authorizedAlways {
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.allowsBackgroundLocationUpdates = true
            locationManager.pausesLocationUpdatesAutomatically = false
            locationManager.startMonitoringSignificantLocationChanges()
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        guard let location = locations.last else { return }
        mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
}

// MARK: - GMSMapViewDelegate
extension DirectionsGoogleMapsController: GMSMapViewDelegate {

}

// MARK: - Setup UI
extension DirectionsGoogleMapsController {
    private func setupUI() {
        view.addSubview(mapView)
        mapView.anchor(top: navBar.bottomAnchor,
                       leading: view.leadingAnchor,
                       bottom: view.bottomAnchor,
                       trailing: view.trailingAnchor)

        view.addSubview(activityController)
        activityController.centerInSuperview()

        view.addSubview(backButton)
        backButton.anchor(top: navBar.bottomAnchor,
                          leading: nil,
                          bottom: nil,
                          trailing: view.trailingAnchor,
                          padding: .init(top: 20, left: 0, bottom: 0, right: 20))
    }

    private func setupNavBar() {
        view.addSubview(navBar)
        navBar.layer.opacity = 0.5
        navBar.layer.shadowRadius = 5
        navBar.anchor(top: view.topAnchor,
                      leading: view.leadingAnchor,
                      bottom: view.safeAreaLayoutGuide.topAnchor,
                      trailing: view.trailingAnchor,
                      padding: .init(top: 0, left: 0, bottom: -100, right: 0))

        let containerView = UIView()
        navBar.addSubview(containerView)
        containerView.anchor(top: view.safeAreaLayoutGuide.topAnchor,
                             leading: navBar.leadingAnchor,
                             bottom: navBar.bottomAnchor,
                             trailing: navBar.trailingAnchor)

        startTextField.attributedPlaceholder = .init(
            string: "Стартовая точка",
            attributes: [.foregroundColor: UIColor.init(white: 1, alpha: 0.7)])
        endTextField.attributedPlaceholder = .init(
            string: "Конечная точка",
            attributes: [.foregroundColor: UIColor.init(white: 1, alpha: 0.7)])
        [startTextField, endTextField].forEach { (textfield) in
            textfield.backgroundColor = .init(white: 1, alpha: 0.3)
            textfield.textColor = .white
        }

        let startHorizontalSV = UIStackView(arrangedSubviews: [
            startIcon, startTextField
        ])
        startHorizontalSV.spacing = 16
        let endHorizontalSV = UIStackView(arrangedSubviews: [
            endIcon, endTextField
        ])
        endHorizontalSV.spacing = 16

        let verticalStackView = UIStackView(arrangedSubviews: [
            startHorizontalSV, endHorizontalSV
        ])
        verticalStackView.axis = .vertical
        verticalStackView.spacing = 12
        verticalStackView.distribution = .fillEqually
        containerView.addSubview(verticalStackView)
        verticalStackView.fillSuperview(padding: .init(top: 0, left: 16, bottom: 12, right: 16))
    }

    private func setupShowRouteButton() {
        view.addSubview(showRouteButton)
        showRouteButton.anchor(top: nil,
                               leading: view.leadingAnchor,
                               bottom: view.bottomAnchor,
                               trailing: view.trailingAnchor,
                               padding: .init(top: 16, left: 16, bottom: 16, right: 16),
                               size: .init(width: 0, height: 50))
    }
}
