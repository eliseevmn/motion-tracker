import UIKit
import GoogleMaps
import CoreLocation

class SearchPlacesGoogleMapViewController: UIViewController {

    // MARK: - Outlets
    private lazy var mapView: GMSMapView = {
        let map = GMSMapView.init(frame: self.view.frame)
        return map
    }()
    private let searchTextField: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Search places"
        return textfield
    }()
    private let backButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(handleBack), for: .touchUpInside)
        button.setImage(#imageLiteral(resourceName: "backIcon"), for: .normal)
        return button
    }()

    // MARK: - Properties
    private var googleClient: GoogleClientRequest = GoogleClient()
    private let locationManager = CLLocationManager()
    private var location = CLLocation()
    private var listener: Any?
    var searchRadius: Int = 2500

    // MARK: - View lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        mapViewSetting()
        searchPlaces()

        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tapGesture)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }

    // MARK: - Map functions
    private func mapViewSetting() {
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
    }

    func createMarker(titleMarker: String, iconMarker: UIImage, coordinate: CLLocationCoordinate2D) {
        let marker = GMSMarker()
        marker.position = coordinate
        marker.title = titleMarker
        marker.icon = iconMarker
        marker.map = mapView
    }

    // MARK: - Private functions
    private func searchPlaces() {
        listener = NotificationCenter.default
            .publisher(for: UITextField.textDidChangeNotification, object: searchTextField)
            .debounce(for: .milliseconds(500), scheduler: RunLoop.main)
            .sink { (text) in
                self.fetchGoogleData(forLocation: self.location, locationName: self.searchTextField.text ?? "", searchRadius: 2500)
        }
    }
}

// MARK: - Actions
extension SearchPlacesGoogleMapViewController {
    @objc private func handleBack() {
        navigationController?.popViewController(animated: true)
    }

    @objc private func dismissKeyboard() {
        view.endEditing(true)
    }
}

// MARK: - Fetch places
extension SearchPlacesGoogleMapViewController {
    func fetchGoogleData(forLocation: CLLocation, locationName: String, searchRadius: Int) {
        googleClient.getGooglePlacesData(forKeyword: locationName, location: forLocation, withinMeters: searchRadius) { (response) in
            self.printPlaces(places: response.results)
            response.results.forEach { (place) in
                self.createMarker(
                    titleMarker: place.name,
                    iconMarker: #imageLiteral(resourceName: "restaurant"),
                    coordinate: CLLocationCoordinate2D(
                        latitude: place.geometry.location.latitude,
                        longitude: place.geometry.location.longitude))
            }
        }
    }

    func printPlaces(places: [GooglePlace]) {
        for place in places {
            print("*******NEW PLACE********")
            let name = place.name
            let address = place.vicinity
            let location = ("lat: \(place.geometry.location.latitude), lng: \(place.geometry.location.longitude)")
            print("\(name), located at \(address), \(location)")
        }
    }
}

// MARK: - CLLocationManagerDelegate
extension SearchPlacesGoogleMapViewController: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse || status == .authorizedAlways {
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.allowsBackgroundLocationUpdates = true
            locationManager.pausesLocationUpdatesAutomatically = false
            locationManager.startMonitoringSignificantLocationChanges()
            mapView.settings.myLocationButton = true
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        guard let location = locations.last else { return }
        mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
        self.location = location
        locationManager.stopUpdatingLocation()
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
}

// MARK: - Setup UI
extension SearchPlacesGoogleMapViewController {
    private func setupUI() {
        view.addSubview(mapView)
        mapView.fillSuperview()

        let container = UIView()
        view.addSubview(container)
        view.addSubview(backButton)

        backButton.anchor(top: view.safeAreaLayoutGuide.topAnchor,
                          leading: view.leadingAnchor,
                          bottom: nil,
                          trailing: nil,
                          padding: .init(top: 8, left: 8, bottom: 8, right: 0),
                          size: .init(width: 34, height: 34))

        container.backgroundColor = .white
        container.anchor(top: view.safeAreaLayoutGuide.topAnchor,
                         leading: backButton.trailingAnchor,
                         bottom: nil,
                         trailing: view.trailingAnchor,
                         padding: .init(top: 0, left: 16, bottom: 0, right: 16),
                         size: .init(width: 0, height: 50))
        container.addSubview(searchTextField)
        searchTextField.fillSuperview(padding: .init(top: 16, left: 16, bottom: 16, right: 16))
    }
}
