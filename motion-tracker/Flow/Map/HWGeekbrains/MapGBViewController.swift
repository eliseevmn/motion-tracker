import UIKit
import GoogleMaps
import CoreLocation
import RxSwift

/// Экран определения маршрута
class MapGBViewController: UIViewController {

    // MARK: - Outlets
    private lazy var mapView: GMSMapView = {
        let map = GMSMapView.init(frame: self.view.frame)
        return map
    }()

    private let currentLocationButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "location"), for: .normal)
        button.addTarget(self, action: #selector(handleCurrenLocation), for: .touchUpInside)
        button.widthAnchor.constraint(equalToConstant: 40).isActive = true
        button.heightAnchor.constraint(equalToConstant: 40).isActive = true
        return button
    }()

    private let switchStartStopRouteButton: CustomButton = {
        let button = CustomButton(title: "Начать новый трек")
        button.addTarget(self, action: #selector(handleStartStropUpdateRoute), for: .touchUpInside)
        return button
    }()

    private let previousRouteButton: CustomButton = {
        let button = CustomButton(title: "Отобразить предыдущий маршрут")
        button.addTarget(self, action: #selector(handlePreviousButton), for: .touchUpInside)
        return button
    }()

    // MARK: - Properties
    private var isRouteButtonStatus: Bool = true
    private var isRoutePreviuosButtonStatus: Bool = false
    var locationManager: LocationManager?
    private let disposeBag = DisposeBag()

    // MARK: - ViewController lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()

        configureMapAndUI()
        configureNavBar()
        mapView.isMyLocationEnabled = true

        locationManager = LocationManager(location: locationManager?.location.value ?? CLLocation(), mapView: mapView)
        MapServiceStyle.configureMapStyle(mapView: mapView)
        configureLocationManager()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = false
    }

    // MARK: - Private function
    private func configureLocationManager() {
        locationManager?
            .location
            .asObservable()
            .bind { [weak self] location  in
                guard let location = location else { return }
                self?.locationManager?.configureCameraMap(coordinate: location.coordinate)
        }
        .disposed(by: disposeBag)
    }

    // MARK: - Configure location
    @objc private func handleCurrenLocation() {
        locationManager?.configureCameraMap(coordinate: locationManager?.location.value?.coordinate)
    }

    // MARK: - Update Route
    @objc private func handleStartStropUpdateRoute() {
        if isRouteButtonStatus {
            startRouteTracking()
            isRouteButtonStatus = false
            isRoutePreviuosButtonStatus = true
        } else {
            stopRouteTracking()
            isRouteButtonStatus = true
            isRoutePreviuosButtonStatus = false
        }
    }

    private func startRouteTracking() {
        previousRouteButton.isEnabled = true
        previousRouteButton.layer.opacity = 1

        switchStartStopRouteButton.setTitle("Закончить трек", for: .normal)
        locationManager?.startRouteTracking()
    }

    private func stopRouteTracking() {
        switchStartStopRouteButton.setTitle("Начать новый трек", for: .normal)
        locationManager?.stopRouteTracking()
    }

    // MARK: - Load Previuos RoutePath
    @objc private func handlePreviousButton() {
        if isRoutePreviuosButtonStatus {
            showStopTrackingAlert()
            locationManager?.stopUpdatingLocation()
            isRoutePreviuosButtonStatus = false
        } else {
            previousRouteButton.isEnabled = false
            previousRouteButton.layer.opacity = 0.2

            locationManager?.loadPreviousRoute()
            locationManager?.loadLastRoute()
        }
    }

    private func showStopTrackingAlert() {
        let alertController = UIAlertController(title: "Отслеживание включено!",
                                                message: "Отключить отслеживание и посмотреть предыдущий маршрут?",
                                                preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: "ОК", style: .destructive) { [weak self] _ in
            self?.previousRouteButton.isEnabled = false
            self?.previousRouteButton.layer.opacity = 0.2

            self?.stopRouteTracking()
            self?.locationManager?.loadPreviousRoute()
            self?.locationManager?.loadLastRoute()
        }
        isRouteButtonStatus = true

        alertController.addAction(confirmAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { [weak self] _ in
            self?.stopRouteTracking()
        }
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
}

// MARK: - Setup UI
extension MapGBViewController {
    private func configureMapAndUI() {
        view.addSubview(mapView)
        view.addSubview(currentLocationButton)
        currentLocationButton.anchor(top: view.topAnchor,
                                leading: nil,
                                bottom: nil,
                                trailing: view.trailingAnchor,
                                padding: .init(top: 100, left: 0, bottom: 0, right: 40))

        let buttonsStackView = UIStackView(arrangedSubviews: [
            previousRouteButton, switchStartStopRouteButton
        ])
        view.addSubview(buttonsStackView)
        buttonsStackView.anchor(top: nil,
                                leading: view.leadingAnchor,
                                bottom: view.bottomAnchor,
                                trailing: view.trailingAnchor,
                                padding: .init(top: 0, left: 20, bottom: 40, right: 20))
        buttonsStackView.axis = .vertical
        buttonsStackView.spacing = 10
    }

    private func configureNavBar() {
        navigationItem.title = "Трекер перемещений"
    }
}
