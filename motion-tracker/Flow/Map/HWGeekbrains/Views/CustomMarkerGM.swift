import UIKit
import GoogleMaps

class CustomMarkerGM: UIView {

    // MARK: - Properties
    var borderColor: UIColor!

    // MARK: - Init
    init(frame: CGRect, borderColor: UIColor) {
        super.init(frame: frame)
        self.borderColor = borderColor
        setupViews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Setup UI
    func setupViews() {
        let imageView = UIImageView()
        addSubview(imageView)
        imageView.anchor(top: topAnchor,
                         leading: leadingAnchor,
                         bottom: nil,
                         trailing: trailingAnchor)
        imageView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        imageView.layer.cornerRadius = 25
        imageView.layer.borderColor = borderColor?.cgColor
        imageView.contentMode = .scaleAspectFill
        imageView.layer.borderWidth = 4
        imageView.clipsToBounds = true
        imageView.image = imageView.getImage(name: "logo")

        let triangleImageView = UIImageView()
        self.insertSubview(triangleImageView, belowSubview: imageView)
        triangleImageView.centerXInSuperview()
        triangleImageView.widthAnchor.constraint(equalToConstant: 23/2).isActive = true
        triangleImageView.heightAnchor.constraint(equalToConstant: 24/2).isActive = true
        triangleImageView.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: -6).isActive = true
        triangleImageView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        triangleImageView.image = UIImage(named: "markerTriangle")
        triangleImageView.tintColor = .white
    }
}
