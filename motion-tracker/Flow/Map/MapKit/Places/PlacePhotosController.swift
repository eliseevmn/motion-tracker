import UIKit

class PlacePhotosController: UIViewController {

    // MARK: - Outlets
    private let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())

    // MARK: - Properties
    var images: [UIImage] = []

    // MARK: - View lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionViewSettings()
    }
}

// MARK: - UICollectionViewDelegate, UICollectionViewDataSource
extension PlacePhotosController: UICollectionViewDelegate, UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: PlacePhotoCell.reuseId,
            for: indexPath) as? PlacePhotoCell else { return UICollectionViewCell()}
        let image = images[indexPath.row]
        cell.image = image

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
    }
}

// MARK: - SetupUI
extension PlacePhotosController {
    private func setupCollectionViewSettings() {
        collectionView.backgroundColor = .white
        view.addSubview(collectionView)
        collectionView.fillSuperview()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(PlacePhotoCell.self, forCellWithReuseIdentifier: PlacePhotoCell.reuseId)
    }
}
