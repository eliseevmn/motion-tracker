import UIKit

class PlacePhotoCell: UICollectionViewCell {

    // MARK: - Properties
    static let reuseId = "PlacePhotoCell"

    var image: UIImage! {
        didSet {
            imageView.image = image
        }
    }

    // MARK: - Outlets
    let imageView = UIImageView(image: nil, contentMode: .scaleAspectFill, width: 0, tintColor: nil)

    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(imageView)
        imageView.fillSuperview()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
