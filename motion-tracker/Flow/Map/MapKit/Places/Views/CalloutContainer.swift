import UIKit

class CalloutContainer: UIView {

    // MARK: - Outlets
    let imageView = UIImageView(image: nil, contentMode: .scaleAspectFill, width: 0, tintColor: nil)
    let nameLabel = UILabel()
    let activityIndicator = UIActivityIndicatorView(style: .large)

    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
        setupShadow()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Setup UI
    private func setupUI() {
        translatesAutoresizingMaskIntoConstraints = false

        backgroundColor = .white
        layer.borderWidth = 2
        layer.borderColor = UIColor.darkGray.cgColor
        layer.cornerRadius = 5

        nameLabel.textAlignment = .center

        activityIndicator.color = .darkGray
        activityIndicator.startAnimating()
        addSubview(activityIndicator)
        activityIndicator.fillSuperview()

        addSubview(imageView)
        imageView.layer.cornerRadius = 5
        imageView.fillSuperview()

        let labelContainer = UIView()
        labelContainer.backgroundColor = .white
        labelContainer.addSubview(nameLabel)
        labelContainer.heightAnchor.constraint(equalToConstant: 30).isActive = true
        nameLabel.fillSuperview(padding: .init(top: 5, left: 10, bottom: 5, right: 10))

        let stackView = UIStackView(arrangedSubviews: [
            UIView(), labelContainer
        ])
        addSubview(stackView)
        stackView.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor)
    }

    private func setupShadow() {
        layer.shadowOpacity = 0.2
        layer.shadowRadius = 5
        layer.shadowOffset = .zero
        layer.shadowColor = UIColor.darkGray.cgColor
    }
}
