import UIKit
import GooglePlaces
import CoreLocation
import MapKit

class PlacesMapKitViewController: UIViewController {

    // MARK: - Outlets
    private let mapView = MKMapView()
    private var currentCustomCallout: UIView?
    private let activityIndicator = UIActivityIndicatorView(style: .large)

    private let hudNameLabel = UILabel(text: "Name", font: .boldSystemFont(ofSize: 16))
    private let hudAddressLabel = UILabel(text: "Address", font: .systemFont(ofSize: 16))
    private let hudTypesLabel = UILabel(text: "Types", textColor: .gray)
    private var infoButton = UIButton(type: .infoLight)
    private let hudContainer = UIView()

    // MARK: - Properties
    private let locationManager = CLLocationManager()
    private let client = GMSPlacesClient()
    private let dispatchGroup = DispatchGroup()

    // MARK: - View lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()

        mapView.delegate = self
        mapView.showsUserLocation = true
        locationManager.delegate = self
        requestForLocationAuthorization()

        setupSelectedAnnotationHUD()
    }

    // Private funcions
    private func setupSelectedAnnotationHUD() {
        infoButton.addTarget(self, action: #selector(handleInformation), for: .touchUpInside)
        infoButton.widthAnchor.constraint(equalToConstant: 44).isActive = true
        view.addSubview(hudContainer)
        hudContainer.layer.cornerRadius = 5
        hudContainer.layer.shadowOpacity = 0.2
        hudContainer.layer.shadowRadius = 5
        hudContainer.layer.shadowColor = UIColor.darkGray.cgColor
        hudContainer.layer.shadowOffset = .zero
        hudContainer.backgroundColor = .white
        hudContainer.anchor(top: nil,
                            leading: view.leadingAnchor,
                            bottom: view.bottomAnchor,
                            trailing: view.trailingAnchor,
                            padding: .init(top: 16, left: 16, bottom: 16, right: 16),
                            size: .init(width: 0, height: 125))
        let topRowStackView = UIStackView(arrangedSubviews: [
            hudNameLabel, infoButton
        ])
        let verticalStackView = UIStackView(arrangedSubviews: [
            topRowStackView, hudAddressLabel, hudTypesLabel
        ])
        verticalStackView.spacing = 8
        verticalStackView.axis = .vertical
        verticalStackView.distribution = .equalCentering
        hudContainer.addSubview(verticalStackView)
        verticalStackView.fillSuperview(padding: .init(top: 16, left: 16, bottom: 16, right: 16))
    }

    private func findNearbyPlaces() {
        client.currentPlace { (likelihoodlist, error) in
            if let error = error {
                print("Failed to find curent place: ", error.localizedDescription)
                return
            }

            likelihoodlist?.likelihoods.forEach({ [weak self] (likelihood) in
                let place = likelihood.place
                let annotation = PlaceAnnotation(place: place)
                annotation.title = place.name
                annotation.coordinate = place.coordinate
                self?.mapView.addAnnotation(annotation)
            })
        }
        mapView.showAnnotations(mapView.annotations, animated: false)
    }
}

// MARK: - Actions
extension PlacesMapKitViewController {
    @objc private func handleInformation() {
        guard let placeAnnotation = mapView.selectedAnnotations.first as? PlaceAnnotation else { return }
        view.addSubview(activityIndicator)
        activityIndicator.fillSuperview()

        guard let placeId = placeAnnotation.place.placeID else { return }

        client.lookUpPhotos(forPlaceID: placeId) { [weak self] (list, error) in
            if let error = error {
                self?.activityIndicator.stopAnimating()
                print("Failed look up photos: ", error)
                return
            }

            var images = [UIImage]()

            list?.results.forEach({ (photoMetadata) in
                self?.dispatchGroup.enter()

                self?.client.loadPlacePhoto(photoMetadata, callback: { (image, error) in
                    if let error = error {
                        print("Failed load image: ", error)
                        return
                    }
                    self?.dispatchGroup.leave()
                    guard let image = image else { return }
                    images.append(image)
                })
            })

            self?.dispatchGroup.notify(queue: .main) {
                self?.activityIndicator.stopAnimating()
                let controller = PlacePhotosController()
                controller.images = images
                self?.present(UINavigationController(rootViewController: controller), animated: true, completion: nil)
            }
        }
    }

    private func setupHUD(view: MKAnnotationView) {
        guard let annotation = view.annotation as? PlaceAnnotation else { return }

        let place = annotation.place
        hudNameLabel.text = place.name
        hudAddressLabel.text = place.formattedAddress
        hudTypesLabel.text = place.types?.joined(separator: ", ")
    }
}

// MARK: - Setup UI
extension PlacesMapKitViewController {
    private func setupUI() {
        view.addSubview(mapView)
        mapView.fillSuperview()
    }
}

// MARK: - MKMapViewDelegate
extension PlacesMapKitViewController: MKMapViewDelegate {

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if !(annotation is PlaceAnnotation) { return nil }

        let annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "id")
        if let placeAnnotation = annotation as? PlaceAnnotation {
            let types = placeAnnotation.place.types
            if let firstType = types?.first {
                if firstType == "bar" {
                    annotationView.image = #imageLiteral(resourceName: "bar")
                } else if firstType == "restaurant" {
                    annotationView.image = #imageLiteral(resourceName: "restaurant")
                } else {
                    annotationView.image = #imageLiteral(resourceName: "default")
                }
            }
        }

        return annotationView
    }

    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        setupHUD(view: view)

        currentCustomCallout?.removeFromSuperview()

        let customCalloutContainer = CalloutContainer()
        view.addSubview(customCalloutContainer)
        let widthAnchor = customCalloutContainer.widthAnchor.constraint(equalToConstant: 200)
        widthAnchor.isActive = true
        let heightAnchor = customCalloutContainer.heightAnchor.constraint(equalToConstant: 100)
        heightAnchor.isActive = true
        customCalloutContainer.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        customCalloutContainer.bottomAnchor.constraint(equalTo: view.topAnchor).isActive = true

        currentCustomCallout = customCalloutContainer

        guard let firstPhotoMetada = (view.annotation as? PlaceAnnotation)?.place.photos?.first else { return }
        client.loadPlacePhoto(firstPhotoMetada) { [weak self] (image, error) in
            if let error = error {
                print("Failed to load photo for place: ", error)
                return
            }

            guard let image = image else { return }
            guard let bestSize = self?.bestCalloutImageSize(image: image) else { return }
            widthAnchor.constant = bestSize.width
            heightAnchor.constant = bestSize.height

            customCalloutContainer.imageView.image = image
            customCalloutContainer.nameLabel.text = (view.annotation as? PlaceAnnotation)?.place.name
        }
    }

    private func bestCalloutImageSize(image: UIImage) -> CGSize {
        if image.size.width > image.size.height {
            let newWidth: CGFloat = 300
            let newHeight = newWidth/image.size.width * image.size.height
            return .init(width: newWidth, height: newHeight)
        } else {
            let newHeight: CGFloat = 200
            let newWidth = newHeight / image.size.height * image.size.width
            return .init(width: newWidth, height: newHeight)
        }
    }
}

// MARK: - CLLocationManagerDelegate
extension PlacesMapKitViewController: CLLocationManagerDelegate {

    func requestForLocationAuthorization() {
        locationManager.requestAlwaysAuthorization()
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else { return }
        let span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
        let region = MKCoordinateRegion(center: location.coordinate, span: span)
        mapView.setRegion(region, animated: true)

        findNearbyPlaces()
        locationManager.stopUpdatingLocation()
    }
}
