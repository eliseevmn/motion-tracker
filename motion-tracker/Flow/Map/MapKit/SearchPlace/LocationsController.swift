import UIKit
import MapKit

class LocationsController: UIViewController {

    // MARK: - Outlets
    var collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout()
    )

    // MARK: - Properties
    var mapItems: [MKMapItem] = []
    weak var mainController: SearchPlaceController?

    // MARK: - View lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionViewSettings()
    }
}

// MARK: - UICollectionViewDataSource, UICollectionViewDelegate
extension LocationsController: UICollectionViewDelegate, UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mapItems.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: LocationCell.reuseId,
            for: indexPath) as? LocationCell else { return UICollectionViewCell()}
        let item = mapItems[indexPath.row]
        cell.mapItem = item

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let annotations = mainController?.mapView.annotations

        annotations?.forEach({ (annotation) in
            guard let custtomAnnotation = annotation as? CustomMapItemAnnotation else { return }

            let mapItem = self.mapItems[indexPath.row]
            if custtomAnnotation.mapItem?.name == mapItem.name { mainController?.mapView.selectAnnotation(annotation, animated: true)
            }
        })

        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
}

// MARk: - UICollectionViewDelegateFlowLayout
extension LocationsController: UICollectionViewDelegateFlowLayout {
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAt section: Int) -> UIEdgeInsets {
        return .init(top: 0, left: 16, bottom: 0, right: 16)
    }

    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: view.frame.width - 64, height: view.frame.height)
    }

    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 12
    }
}

// MARK: - SetupUI
extension LocationsController {
    private func setupCollectionViewSettings() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .clear
        view.addSubview(collectionView)
        collectionView.fillSuperview()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.clipsToBounds = false
        collectionView.register(LocationCell.self, forCellWithReuseIdentifier: LocationCell.reuseId)
    }
}
