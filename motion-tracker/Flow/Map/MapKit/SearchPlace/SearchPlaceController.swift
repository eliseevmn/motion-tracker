import UIKit
import MapKit

class SearchPlaceController: UIViewController {

    // MARK: - Outlets
    let mapView = MKMapView()
    private let searchTextField: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Search places"
        return textfield
    }()
    private let backButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(handleBack), for: .touchUpInside)
        button.setImage(#imageLiteral(resourceName: "backIcon"), for: .normal)
        return button
    }()

    // MARK: - Properties
    private let locationManager = CLLocationManager()
    private let locationsController = LocationsController()
    private var listener: Any?

    // MARK: - View lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()

        requestUserLocation()
        setupMapViewSettings()

        setupUI()
        setupLocationsController()
        locationsController.mainController = self
        locationManager.stopUpdatingLocation()

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tapGesture)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }

    // MARK: - MapsView Settings
    private func setupMapViewSettings() {
        mapView.delegate = self
        mapView.showsUserLocation = true
    }

    // MARK: - Private functions
    private func localSearch() {
        let request = MKLocalSearch.Request()
        request.naturalLanguageQuery = searchTextField.text
        request.region = mapView.region

        let localSearch = MKLocalSearch(request: request)
        localSearch.start { (response, error) in
            if let error = error {
                print("Failed local search: ", error)
                return
            }

            self.mapView.removeAnnotations(self.mapView.annotations)
            self.locationsController.mapItems.removeAll()

//            print(response?.mapItems.count)
            response?.mapItems.forEach({ (mapItem) in
                let annotation = CustomMapItemAnnotation()
                annotation.mapItem = mapItem
                annotation.coordinate = mapItem.placemark.coordinate
                annotation.title = "Location: " + (mapItem.name ?? "")
                self.mapView.addAnnotation(annotation)

                self.locationsController.mapItems.append(mapItem)
            })

            DispatchQueue.main.async {
                self.locationsController.collectionView.reloadData()
            }
        }

        self.locationsController.collectionView.scrollToItem(at: [0, 0], at: .centeredHorizontally, animated: true)

        self.mapView.showAnnotations(self.mapView.annotations, animated: true)
    }
}

// MARk: - Actions
extension SearchPlaceController {
    @objc private func handleBack() {
        navigationController?.popViewController(animated: true)
    }

    @objc private func dismissKeyboard() {
        view.endEditing(true)
    }
}

// MARK: - MKMapViewDelegate
extension SearchPlaceController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let customAnnotation = view.annotation as? CustomMapItemAnnotation else { return }

        guard let index = self.locationsController.mapItems.firstIndex(where: { $0.name == customAnnotation.mapItem?.name }) else { return }
        self.locationsController.collectionView.scrollToItem(at: [0, index], at: .centeredHorizontally, animated: true)
    }

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is CustomMapItemAnnotation {
            let annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "id")
            annotationView.canShowCallout = true
            annotationView.image = #imageLiteral(resourceName: "restaurant")
            return annotationView
        }
        return nil
    }
}

// MARK: - CLLocationManagerDelegate
extension SearchPlaceController: CLLocationManagerDelegate {
    private func requestUserLocation() {
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else { return }
        let coordinate = location.coordinate
        let span = MKCoordinateSpan(latitudeDelta: 0.2, longitudeDelta: 0.2)
        let region = MKCoordinateRegion(center: coordinate, span: span)
        mapView.setRegion(region, animated: true)
    }
}

// MARK: - Setup UI
extension SearchPlaceController {
    private func setupUI() {
        view.addSubview(mapView)
        mapView.fillSuperview()
        mapView.mapType = .mutedStandard

        setupSearchUI()
    }

    private func setupSearchUI() {
        let container = UIView()
        view.addSubview(container)
        view.addSubview(backButton)

        backButton.anchor(top: view.safeAreaLayoutGuide.topAnchor,
                          leading: view.leadingAnchor,
                          bottom: nil,
                          trailing: nil,
                          padding: .init(top: 8, left: 8, bottom: 8, right: 0),
                          size: .init(width: 34, height: 34))

        container.backgroundColor = .white
        container.anchor(top: view.safeAreaLayoutGuide.topAnchor,
                         leading: backButton.trailingAnchor,
                         bottom: nil,
                         trailing: view.trailingAnchor,
                         padding: .init(top: 0, left: 16, bottom: 0, right: 16),
                         size: .init(width: 0, height: 50))
        container.addSubview(searchTextField)
        searchTextField.fillSuperview(padding: .init(top: 16, left: 16, bottom: 16, right: 16))

        listener = NotificationCenter.default
            .publisher(for: UITextField.textDidChangeNotification, object: searchTextField)
            .debounce(for: .milliseconds(500), scheduler: RunLoop.main)
            .sink { (_) in
                self.localSearch()
        }
    }

    private func setupLocationsController() {
        let locationsView = locationsController.view!
        view.addSubview(locationsView)
        locationsView.anchor(top: nil,
                             leading: view.leadingAnchor,
                             bottom: view.safeAreaLayoutGuide.bottomAnchor,
                             trailing: view.trailingAnchor,
                             padding: .init(top: 0, left: 0, bottom: 20, right: 0),
                             size: .init(width: 0, height: 100))
    }
}
