import UIKit
import MapKit

class LocationCell: UICollectionViewCell {

    // MARK: - Properties
    static let reuseId = "LocationCell"

    var mapItem: MKMapItem! {
        didSet {
            label.text = mapItem.name
            addressLabel.text = mapItem.address()
        }
    }

    // MARK: - Outlets
    let label = UILabel(text: "Местонахождение", font: .boldSystemFont(ofSize: 16))
    let addressLabel = UILabel(text: "Адрес", font: nil)

    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        addressLabel.numberOfLines = 0
        backgroundColor = .white
        layer.shadowOpacity = 0.2
        layer.shadowRadius = 5
        layer.shadowOffset = .zero
        layer.shadowColor = UIColor.black.cgColor
        layer.cornerRadius = 16

        let verticalStackView = UIStackView(arrangedSubviews: [
            label, addressLabel
        ])
        verticalStackView.spacing = 5
        verticalStackView.axis = .vertical
        verticalStackView.distribution = .equalCentering

        addSubview(verticalStackView)
        verticalStackView.fillSuperview(padding: .init(top: 10, left: 10, bottom: 10, right: 10))
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
