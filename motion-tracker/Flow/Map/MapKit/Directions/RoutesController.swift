import UIKit
import MapKit

class RoutesController: UIViewController {

    // MARK: - Outlets
    private let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())

    // MARK: - Properties
    var route: MKRoute?
    var steps: [MKRoute.Step] = []

    // MARK: - View lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionViewSettings()
    }
}

// MARK: - SetupUI
extension RoutesController {
    private func setupCollectionViewSettings() {
        collectionView.backgroundColor = .white
        view.addSubview(collectionView)
        collectionView.fillSuperview()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(RouteCell.self, forCellWithReuseIdentifier: RouteCell.resueId)
        collectionView.register(
            RouterHeader.self,
            forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
            withReuseIdentifier: RouterHeader.reuseID)
    }
}

// MARK: - UICollectionViewDelegate, UICollectionViewDataSource
extension RoutesController: UICollectionViewDelegate, UICollectionViewDataSource {

    func collectionView(
        _ collectionView: UICollectionView,
        viewForSupplementaryElementOfKind kind: String,
        at indexPath: IndexPath) -> UICollectionReusableView {
        guard let header = collectionView.dequeueReusableSupplementaryView(
            ofKind: kind,
            withReuseIdentifier: RouterHeader.reuseID,
            for: indexPath) as? RouterHeader else { return UICollectionReusableView() }

        guard let route = route else { return UICollectionReusableView() }
        header.configureHeader(route: route)

        return header
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return steps.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: RouteCell.resueId,
            for: indexPath) as? RouteCell else { return UICollectionViewCell()}

        let item = steps[indexPath.item]
        cell.routeItem = item

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension RoutesController: UICollectionViewDelegateFlowLayout {
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: view.frame.width, height: 70)
    }

    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        referenceSizeForHeaderInSection section: Int) -> CGSize {
        return .init(width: 0, height: 120)
    }
}
