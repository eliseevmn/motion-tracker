import UIKit
import MapKit

class LocationSearchCell: UICollectionViewCell {

    // MARK: - Properties
    static let resueId = "LocationSearchCell"

    var mapItem: MKMapItem! {
        didSet {
            nameLabel.text = mapItem.name
            addressLabel.text = mapItem.address()
        }
    }

    // MARK: - Outlets
    private let nameLabel = UILabel(text: "Name", font: .boldSystemFont(ofSize: 16))
    private let addressLabel = UILabel(text: "Address", font: .systemFont(ofSize: 14))

    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Setup UI
    private func setupUI() {
        let separatorView = UIView()
        separatorView.backgroundColor = UIColor(white: 0.6, alpha: 0.5)

        let stackView = UIStackView(arrangedSubviews: [
            nameLabel, addressLabel
        ])
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        addSubview(stackView)
        addSubview(separatorView)
        stackView.fillSuperview(padding: .init(top: 8, left: 16, bottom: 8, right: 16))
        separatorView.anchor(top: nil,
                             leading: leadingAnchor,
                             bottom: bottomAnchor,
                             trailing: trailingAnchor,
                             padding: .init(top: 0, left: 16, bottom: 0, right: 16),
                             size: .init(width: 0, height: 0.5))
    }
}
