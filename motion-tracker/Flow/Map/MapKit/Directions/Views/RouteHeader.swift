import UIKit
import MapKit

class RouterHeader: UICollectionReusableView {

    // MARK: - Properties
    static let reuseID = "RouterHeader"

    // MARK: - Oulets
    private var nameLabel = UILabel(text: "Наименование", font: .systemFont(ofSize: 16))
    private let distanceLabel = UILabel(text: "Дистанция", font: .systemFont(ofSize: 16))
    private let estimatedLabel = UILabel(text: "Время в пути...", font: .systemFont(ofSize: 16))

    // Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private functions
    private func setupUI() {
        let stackView = UIStackView(arrangedSubviews: [
            nameLabel,
            distanceLabel,
            estimatedLabel
        ])
        addSubview(stackView)
        stackView.fillSuperview(padding: .init(top: 16, left: 16, bottom: 16, right: 16))
        stackView.spacing = 16
        stackView.alignment = .leading
        stackView.axis = .vertical

        nameLabel.attributedText = generateAttributedString(title: "Route", description: "M7")
        distanceLabel.attributedText = generateAttributedString(title: "Distance", description: "10 км")
        estimatedLabel.attributedText = generateAttributedString(title: "Время", description: "...")
    }

    private func generateAttributedString(title: String, description: String) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: title + ": ", attributes: [.font: UIFont.boldSystemFont(ofSize: 16)])
        attributedString.append(.init(string: description, attributes: [.font: UIFont.systemFont(ofSize: 16)]))
        return attributedString
    }

    func configureHeader(route: MKRoute) {
        nameLabel.attributedText = generateAttributedString(title: "Маршрут", description: route.name)
        let kmDistance = route.distance / 1000
        distanceLabel.attributedText = generateAttributedString(title: "Расстояние", description: "\(kmDistance) км")

        var timeString = ""
        if route.expectedTravelTime > 3600 {
              let hour = Int(route.expectedTravelTime / 60 / 60)
              let minute = Int((route.expectedTravelTime.truncatingRemainder(dividingBy: 60 * 60)) / 60)
              timeString = String(format: " %d hr %d min", hour, minute)
          } else {
              let time = Int(route.expectedTravelTime / 60)
              timeString = String(format: "%d min", time)
          }
          estimatedLabel.attributedText = generateAttributedString(title: "Время в пути:", description: timeString)
    }
}
