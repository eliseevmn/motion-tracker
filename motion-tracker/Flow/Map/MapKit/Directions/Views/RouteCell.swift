import UIKit
import MapKit

class RouteCell: UICollectionViewCell {

    // MARK: - Properties
    static let resueId = "RouteCell"

    var routeItem: MKRoute.Step! {
        didSet {
            nameLabel.text = routeItem.instructions
            let kmDistance = routeItem.distance / 1000
            distanceLabel.text = "\(kmDistance) км"
        }
    }

    // MARK: - Outlets
    private let nameLabel = UILabel(text: "Name", font: .boldSystemFont(ofSize: 16))
    private let distanceLabel = UILabel(text: "Distance", font: .systemFont(ofSize: 14))

    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Setup UI
    private func setupUI() {
        nameLabel.numberOfLines = 0
        distanceLabel.textAlignment = .left

        let separatorView = UIView()
        separatorView.backgroundColor = UIColor(white: 0.6, alpha: 0.5)

        let stackView = UIStackView(arrangedSubviews: [
            nameLabel, distanceLabel
        ])
        stackView.axis = .vertical
        addSubview(stackView)
        addSubview(separatorView)
        stackView.fillSuperview(padding: .init(top: 16, left: 16, bottom: 16, right: 16))
        separatorView.anchor(top: nil,
                             leading: leadingAnchor,
                             bottom: bottomAnchor,
                             trailing: trailingAnchor,
                             padding: .init(top: 0, left: 16, bottom: 0, right: 16),
                             size: .init(width: 0, height: 0.5))
    }
}
