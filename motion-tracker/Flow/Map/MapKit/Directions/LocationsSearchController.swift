import UIKit
import MapKit

class LocationSearchController: UIViewController {

    // MARK: - Outlets
    private let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    private let searchTextField = DirectionsTextField(placeholder: "Введите наименование", padding: 12)
    private let backButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "back_arrow").withTintColor(.white).withRenderingMode(.alwaysTemplate), for: .normal)
        button.widthAnchor.constraint(equalToConstant: 32).isActive = true
        return button
    }()

    // MARK: - Properties
    var items: [MKMapItem] = []
    var selectionHandler: ((MKMapItem) -> Void)?
    private var listener: Any?
    private let navBarHeight: CGFloat = 66

    // MARK: - View lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionViewSettings()
        setupSearchBar()
        setupSearchListener()

        searchTextField.becomeFirstResponder()
        backButton.addTarget(self, action: #selector(handleBack), for: .touchUpInside)
    }

    // MARK: - Search functions
    private func setupSearchListener() {
        listener = NotificationCenter.default.publisher(
            for: UITextField.textDidChangeNotification,
            object: searchTextField)
            .debounce(for: .milliseconds(600), scheduler: RunLoop.main)
            .sink(receiveValue: { [weak self] (_) in
                self?.localSearch()
            })
    }

    private func localSearch() {
        let request = MKLocalSearch.Request()
        request.naturalLanguageQuery = searchTextField.text

        let search = MKLocalSearch.init(request: request)
        search.start { (response, error) in
            if let error = error {
                print("Failed response search: ", error.localizedDescription)
                return
            }

            self.items = response?.mapItems ?? []
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
}

// MARK: - Actions
extension LocationSearchController {
    @objc private func handleBack() {
        navigationController?.popViewController(animated: true)
    }
}

// MARK: - SetupUI
extension LocationSearchController {
    private func setupCollectionViewSettings() {
        collectionView.backgroundColor = .white
        view.addSubview(collectionView)
        collectionView.fillSuperview()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.verticalScrollIndicatorInsets.top = navBarHeight
        collectionView.register(LocationSearchCell.self, forCellWithReuseIdentifier: LocationSearchCell.resueId)
    }

    private func setupSearchBar() {
        let navBar = UIView()
        navBar.backgroundColor = .clear
        view.addSubview(navBar)
        navBar.anchor(top: view.topAnchor,
                      leading: view.leadingAnchor,
                      bottom: view.safeAreaLayoutGuide.topAnchor,
                      trailing: view.trailingAnchor,
                      padding: .init(top: 0, left: 0, bottom: -navBarHeight, right: 0))
        let container = UIView()
        container.backgroundColor = .clear
        navBar.addSubview(container)
        container.anchor(top: view.safeAreaLayoutGuide.topAnchor,
                         leading: navBar.leadingAnchor,
                         bottom: navBar.bottomAnchor,
                         trailing: navBar.trailingAnchor,
                         padding: .init(top: 0, left: 16, bottom: 16, right: 16))
        let stackView = UIStackView(arrangedSubviews: [
            backButton, searchTextField
        ])
        stackView.spacing = 12
        container.addSubview(stackView)
        stackView.fillSuperview()

        searchTextField.layer.borderWidth = 2
        searchTextField.layer.borderColor = UIColor.lightGray.cgColor
        searchTextField.layer.cornerRadius = 5
    }
}

// MARK: - UICollectionViewDelegate, UICollectionViewDataSource
extension LocationSearchController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: LocationSearchCell.resueId,
            for: indexPath) as? LocationSearchCell else { return UICollectionViewCell()}

        let item = items[indexPath.item]
        cell.mapItem = item

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let mapItem = items[indexPath.item]
        selectionHandler?(mapItem)
        handleBack()
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension LocationSearchController: UICollectionViewDelegateFlowLayout {
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .init(top: navBarHeight, left: 0, bottom: 0, right: 0)
    }

    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: view.frame.width, height: 80)
    }
}
