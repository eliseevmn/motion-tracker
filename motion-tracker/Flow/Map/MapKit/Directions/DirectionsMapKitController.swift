import UIKit
import MapKit

class DirectionsMapKitController: UIViewController {

    // MARK: - Outlets
    private let mapView = MKMapView()
    private let navBar: UIView = {
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.2587935925, green: 0.5251715779, blue: 0.9613835216, alpha: 1)
        return view
    }()

    private let startTextField = DirectionsTextField(padding: 12,
                                                     cornerRadius: 5)
    private let endTextField = DirectionsTextField(padding: 12,
                                                   cornerRadius: 5)

    private let showRouteButton = UIButton(title: "Показать маршрут",
                                   titleColor: .black,
                                   backgroundColor: .white,
                                   font: .boldSystemFont(ofSize: 16),
                                   isShadow: true,
                                   cornerRadius: 5)

    private let startIcon = UIImageView(
        image: #imageLiteral(resourceName: "startlocatino").withRenderingMode(.alwaysTemplate),
        contentMode: .scaleAspectFit, width: 20, tintColor: .white)
    private let endIcon = UIImageView(
        image: #imageLiteral(resourceName: "endlocation").withRenderingMode(.alwaysTemplate),
        contentMode: .scaleAspectFit, width: 20, tintColor: .white)
    let activityController = UIActivityIndicatorView(style: .large)
    let backButton = UIButton(title: "Назад", titleColor: .black)

    // MARK: - Properties
    private var startMapItem: MKMapItem?
    private var endMapItem: MKMapItem?
    private var currentShowingRoute: MKRoute?

    // MARK: View lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        setupUI()
        setupShowRouteButton()
        setupMapViewSettings()

        showRouteButton.addTarget(self, action: #selector(handleShowRoute), for: .touchUpInside)
        backButton.addTarget(self, action: #selector(handleBack), for: .touchUpInside)
        startTextField.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleChangeStartLocation)))
        endTextField.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleCnahgeEndLocation)))
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }

    // MARK: Map functions
    private func setupMapViewSettings() {
        mapView.showsUserLocation = true
        mapView.delegate = self
    }

    private func setupRegionForMap(coordinate: CLLocationCoordinate2D) {
        let span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
        let region = MKCoordinateRegion(center: coordinate, span: span)
        mapView.setRegion(region, animated: true)
    }
}

// MARK: - Actions
extension DirectionsMapKitController {
    @objc private func handleBack() {
        self.navigationController?.popViewController(animated: true)
    }

    @objc private func handleShowRoute() {
        let controller = RoutesController()

        if currentShowingRoute != nil {
            controller.route = currentShowingRoute
            controller.steps = currentShowingRoute?.steps.filter { !$0.instructions.isEmpty } ?? []
            present(controller, animated: true, completion: nil)
        }
    }

    @objc private func handleChangeStartLocation() {
        let controller = LocationSearchController()
        controller.selectionHandler = { [weak self] mapItem in
            self?.startTextField.text = mapItem.name
            self?.startMapItem = mapItem
            self?.refreshMap()
        }
        navigationController?.pushViewController(controller, animated: true)
    }

    @objc private func handleCnahgeEndLocation() {
        let controller = LocationSearchController()
        controller.selectionHandler = { [weak self] mapItem in
            self?.endTextField.text = mapItem.name
            self?.endMapItem = mapItem
            self?.refreshMap()
        }
        navigationController?.pushViewController(controller, animated: true)
    }

    private func refreshMap() {
        mapView.removeAnnotations(mapView.annotations)
        mapView.removeOverlays(mapView.overlays)

        if let mapItem = startMapItem {
            let annotation = MKPointAnnotation()
            annotation.coordinate = mapItem.placemark.coordinate
            annotation.title = mapItem.name
            self.mapView.addAnnotation(annotation)
            self.mapView.showAnnotations(mapView.annotations, animated: false)
        }

        if let mapItem = endMapItem {
            let annotation = MKPointAnnotation()
            annotation.coordinate = mapItem.placemark.coordinate
            annotation.title = mapItem.name
            self.mapView.addAnnotation(annotation)
            self.mapView.showAnnotations(mapView.annotations, animated: false)
        }

        self.requestForDirectionsRoute()
    }

    private func requestForDirectionsRoute() {
        let request = MKDirections.Request()
        request.source = startMapItem
        request.destination = endMapItem
        request.transportType = .automobile
//        request.requestsAlternateRoutes = true

        activityController.startAnimating()

        let directions = MKDirections(request: request)
        directions.calculate { [weak self] (response, error) in
            self?.activityController.stopAnimating()

            if let error = error {
                print("Failed ro find route :", error.localizedDescription)
                return
            }

            if let firstRoute = response?.routes.first {
                self?.mapView.addOverlay(firstRoute.polyline)
                self?.currentShowingRoute = firstRoute
            }
        }

    }
}

// MARK: - Location
extension DirectionsMapKitController {
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        setupRegionForMap(coordinate: userLocation.coordinate)
    }
}

// MARK: - Setup UI
extension DirectionsMapKitController {
    private func setupUI() {
        view.addSubview(mapView)
        mapView.anchor(top: navBar.bottomAnchor,
                       leading: view.leadingAnchor,
                       bottom: view.bottomAnchor,
                       trailing: view.trailingAnchor)

        view.addSubview(activityController)
        activityController.centerInSuperview()

        view.addSubview(backButton)
        backButton.anchor(top: navBar.bottomAnchor,
                          leading: nil,
                          bottom: nil,
                          trailing: view.trailingAnchor,
                          padding: .init(top: 20, left: 0, bottom: 0, right: 20))
    }

    private func setupNavBar() {
        view.addSubview(navBar)
        navBar.layer.opacity = 0.5
        navBar.layer.shadowRadius = 5
        navBar.anchor(top: view.topAnchor,
                      leading: view.leadingAnchor,
                      bottom: view.safeAreaLayoutGuide.topAnchor,
                      trailing: view.trailingAnchor,
                      padding: .init(top: 0, left: 0, bottom: -100, right: 0))

        let containerView = UIView()
        navBar.addSubview(containerView)
        containerView.anchor(top: view.safeAreaLayoutGuide.topAnchor,
                             leading: navBar.leadingAnchor,
                             bottom: navBar.bottomAnchor,
                             trailing: navBar.trailingAnchor)

        startTextField.attributedPlaceholder = .init(
            string: "Стартовая точка",
            attributes: [.foregroundColor: UIColor.init(white: 1, alpha: 0.7)])
        endTextField.attributedPlaceholder = .init(
            string: "Конечная точка",
            attributes: [.foregroundColor: UIColor.init(white: 1, alpha: 0.7)])
        [startTextField, endTextField].forEach { (textfield) in
            textfield.backgroundColor = .init(white: 1, alpha: 0.3)
            textfield.textColor = .white
        }

        let startHorizontalSV = UIStackView(arrangedSubviews: [
            startIcon, startTextField
        ])
        startHorizontalSV.spacing = 16
        let endHorizontalSV = UIStackView(arrangedSubviews: [
            endIcon, endTextField
        ])
        endHorizontalSV.spacing = 16

        let verticalStackView = UIStackView(arrangedSubviews: [
            startHorizontalSV, endHorizontalSV
        ])
        verticalStackView.axis = .vertical
        verticalStackView.spacing = 12
        verticalStackView.distribution = .fillEqually
        containerView.addSubview(verticalStackView)
        verticalStackView.fillSuperview(padding: .init(top: 0, left: 16, bottom: 12, right: 16))
    }

    private func setupShowRouteButton() {
        view.addSubview(showRouteButton)
        showRouteButton.anchor(top: nil,
                               leading: view.leadingAnchor,
                               bottom: view.bottomAnchor,
                               trailing: view.trailingAnchor,
                               padding: .init(top: 16, left: 16, bottom: 16, right: 16),
                               size: .init(width: 0, height: 50))
    }
}

// MARK: - MKMapViewDelegate
extension DirectionsMapKitController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let polylineRenderer = MKPolylineRenderer(overlay: overlay)
        polylineRenderer.strokeColor = #colorLiteral(red: 0.2587935925, green: 0.5251715779, blue: 0.9613835216, alpha: 1)
        polylineRenderer.lineWidth = 5
        return polylineRenderer
    }
}
