import UIKit
import RxSwift
import RxCocoa

final class RegistrationViewController: UIViewController {

    // MARK: - Outlets
    private let loginTextField: CustomTextField = {
        let textField = CustomTextField(cornerRadius: 10, height: 50, fontSize: 16, labelText: "Введите свой логин")
        return textField
    }()

    private let passwordTextField: CustomTextField = {
        let textField = CustomTextField(cornerRadius: 10, height: 50, fontSize: 16, labelText: "Введите пароль")
        textField.isSecureTextEntry = true
        return textField
    }()

    private let registrationButton: CustomButton = {
        let button = CustomButton(title: "Зарегистрироваться")
        button.addTarget(self, action: #selector(handleRegistration), for: .touchUpInside)
        return button
    }()

    // MARK: - Properties
    private let disposeBag = DisposeBag()

    // MARK: - ViewController lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white

        setupUI()
        configureRegistrationrBindings()

        let hideKeyboardGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(hideKeyboardGesture)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = false
    }

    // MARK: - Private functions
    private func configureRegistrationrBindings() {
        Observable
            .combineLatest(
                loginTextField.rx.text,
                passwordTextField.rx.text)
            .map { login, password in
                return !(login ?? "").isEmpty
                    && !(password ?? "").isEmpty
                    && (password ?? "").count >= 3 }
            .bind { [weak registrationButton] inputFilled in
                registrationButton?.isEnabled = inputFilled
                self.setContinueSignButton(enabled: inputFilled) }
            .disposed(by: disposeBag)
    }

    private func setContinueSignButton(enabled: Bool) {
        if enabled {
            registrationButton.alpha = 1.0
            registrationButton.isEnabled = true
        } else {
            registrationButton.alpha = 0.5
            registrationButton.isEnabled = false
        }
    }

    private func clearTextfields() {
        loginTextField.text = ""
        passwordTextField.text = ""
    }
}

// Actions
extension RegistrationViewController {
    @objc private func hideKeyboard() {
        view.endEditing(true)
    }

    @objc private func handleRegistration(_ sender: Any) {
        guard let login = loginTextField.text,
            let password = passwordTextField.text else { return }
        let user = User(login: login, password: password)
        if RealmService.saveUser(user: user) {
            showAlert(title: "Уважаемый, \(user.login)",
            message: "Вы успешно зарегистрированы!") { [weak self] _ in
                self?.clearTextfields()
            }
        } else {
            showAlert(title: "Ошибка", message: "Такое пользователь существует")
        }
        print("Все ок")
    }
}

// MARK: - Setup UI
extension RegistrationViewController {
    private func setupUI() {
        registrationButton.alpha = 0.5
        registrationButton.isEnabled = false

        let stackView = UIStackView(arrangedSubviews: [
            loginTextField,
            passwordTextField
        ])
        view.addSubview(registrationButton)
        view.addSubview(stackView)
        stackView.axis = .vertical
        stackView.spacing = 20
        stackView.anchor(top: view.topAnchor,
                         leading: view.leadingAnchor,
                         bottom: nil,
                         trailing: view.trailingAnchor,
                         padding: .init(top: 190, left: 40, bottom: 0, right: 40))
        registrationButton.anchor(top: stackView.bottomAnchor,
                                  leading: view.leadingAnchor,
                                  bottom: nil,
                                  trailing: view.trailingAnchor,
                                  padding: .init(top: 40, left: 40, bottom: 0, right: 40))
    }
}
