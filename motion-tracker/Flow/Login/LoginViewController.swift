import UIKit
import RxSwift
import RxCocoa

final class LoginViewController: UIViewController {

    // MARK: - Outlets
    private let loginTextField: CustomTextField = {
        let textField = CustomTextField(cornerRadius: 10, height: 50, fontSize: 16, labelText: "Введите свой логин")
        return textField
    }()

    private let passwordTextField: CustomTextField = {
        let textField = CustomTextField(cornerRadius: 10, height: 50, fontSize: 16, labelText: "Введите пароль")
        textField.isSecureTextEntry = true
        return textField
    }()

    private let loginButton: CustomButton = {
        let button = CustomButton(title: "Войти")
        button.addTarget(self, action: #selector(handleLogin), for: .touchUpInside)
        return button
    }()

    private let recoveryLoginButton: UIButton = {
        let button = UIButton()
        button.setTitle("Восстановить пароль?", for: .normal)
        button.addTarget(self, action: #selector(handleRecovery), for: .touchUpInside)
        button.setTitleColor(.blue, for: .normal)
        return button
    }()

    private let registrationButton: UIButton = {
        let button = UIButton()
        button.setTitle("Зарегистрироваться", for: .normal)
        button.addTarget(self, action: #selector(handleRegistration), for: .touchUpInside)
        button.setTitleColor(.blue, for: .normal)
        return button
    }()

    // MARK: - Properties
    var onLogin: (() -> Void)?
    var onRecover: (() -> Void)?
    var onRegistration: (() -> Void)?
    private let disposeBag = DisposeBag()

    // MARK: - ViewController lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupUI()

        let hideKeyboardGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(hideKeyboardGesture)
    }

    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
        clearTextfields()
        configureLoginBindings()
    }

    // MARK: - Private functions
    private func clearTextfields() {
        loginTextField.text = ""
        passwordTextField.text = ""
    }

    private func configureLoginBindings() {
        Observable
            .combineLatest(
                loginTextField.rx.text,
                passwordTextField.rx.text)
            .map { login, password in
                return !(login ?? "").isEmpty
                    && !(password ?? "").isEmpty
                    && (password ?? "").count >= 3 }
            .bind { [weak loginButton] inputFilled in
                loginButton?.isEnabled = inputFilled
                self.setContinueSignButton(enabled: inputFilled) }
            .disposed(by: disposeBag)
    }

    private func setContinueSignButton(enabled: Bool) {
        if enabled {
            loginButton.alpha = 1.0
            loginButton.isEnabled = true
        } else {
            loginButton.alpha = 0.5
            loginButton.isEnabled = false
        }
    }
}

// MARK: Actions
extension LoginViewController {
    @objc private func handleLogin(_ sender: Any) {
        guard
            let login = loginTextField.text,
            let password = passwordTextField.text
            else {
                return
        }
        if RealmService.signUser(login: login, password: password) {
            UserDefaults.standard.set(true, forKey: "isLogin")
            onLogin?()
        } else {
            showAlert(title: "Ошибка", message: "Вы ошиблись с логином или паролем")
        }
    }

    @objc private func handleRecovery(_ sender: Any) {
        onRecover?()
    }

    @objc private func handleRegistration() {
        onRegistration?()
    }

    @objc private func hideKeyboard() {
        view.endEditing(true)
    }
}

// MARK: - SetupUI
extension LoginViewController {
    private func setupUI() {
        loginButton.alpha = 0.5
        loginButton.isEnabled = false

        let textFieldStackView = UIStackView(arrangedSubviews: [
            loginTextField,
            passwordTextField
        ])
        textFieldStackView.axis = .vertical
        textFieldStackView.spacing = 20
        let buttonStackView = UIStackView(arrangedSubviews: [
            loginButton,
            recoveryLoginButton,
            registrationButton
        ])
        buttonStackView.axis = .vertical
        buttonStackView.spacing = 20

        let overallStackView = UIStackView(arrangedSubviews: [
            textFieldStackView,
            buttonStackView
        ])

        view.addSubview(overallStackView)
        overallStackView.axis = .vertical
        overallStackView.spacing = 40
        overallStackView.anchor(top: view.topAnchor,
                         leading: view.leadingAnchor,
                         bottom: nil,
                         trailing: view.trailingAnchor,
                         padding: .init(top: 190, left: 40, bottom: 0, right: 40))
    }
}
