import UIKit
import RxSwift
import RxCocoa

final class RecoveryPasswordViewController: UIViewController {

    // MARK: - Outlets
    private let loginTextField: CustomTextField! = {
        let textField = CustomTextField(cornerRadius: 10, height: 50, fontSize: 16, labelText: "Введите свой логин")
        textField.addTarget(self, action: #selector(textFieldChange), for: .editingChanged)
        return textField
    }()

    private let recoveryLoginButton: CustomButton = {
        let button = CustomButton(title: "Восстановить пароль")
        button.addTarget(self, action: #selector(handleRecovery), for: .touchUpInside)
        return button
    }()

    // MARK: - ViewController lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = false
    }

    // MARK: - Private functions
    private func showPassword(login: String) {
        let password = RealmService.recoveryPassword(login: login)
        showAlert(title: "Пароль", message: password)
    }

    private func setContinueSignButton(enabled: Bool) {
        if enabled {
            recoveryLoginButton.alpha = 1.0
            recoveryLoginButton.isEnabled = true
        } else {
            recoveryLoginButton.alpha = 0.5
            recoveryLoginButton.isEnabled = false
        }
    }

    private func clearTextfields() {
        loginTextField.text = ""
    }

    private func checkValid() -> String? {
        if loginTextField.text == "" ||
            loginTextField.text == nil {
            return "Please fill in all fields"
        }
        return nil
    }
}

// MARK: - Actions
extension RecoveryPasswordViewController {
    @objc private func textFieldChange() {
         guard let login = loginTextField.text else { return }
         let formFilled = !(login.isEmpty)
         setContinueSignButton(enabled: formFilled)
     }

    @objc private func handleRecovery(_ sender: Any) {
        guard let login = loginTextField.text else { return }
        showPassword(login: login)
    }
    @objc private func hideKeyboard() {
        view.endEditing(true)
    }
}

// MARK: - Setup UI
extension RecoveryPasswordViewController {
    private func setupUI() {
        recoveryLoginButton.alpha = 0.5
        recoveryLoginButton.isEnabled = false

        let stackView = UIStackView(arrangedSubviews: [
            loginTextField,
            recoveryLoginButton
        ])
        view.addSubview(stackView)
        stackView.axis = .vertical
        stackView.spacing = 40
        stackView.anchor(top: view.topAnchor,
                         leading: view.leadingAnchor,
                         bottom: nil,
                         trailing: view.trailingAnchor,
                         padding: .init(top: 190, left: 40, bottom: 0, right: 40))
    }
}
