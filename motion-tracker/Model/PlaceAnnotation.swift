import Foundation
import MapKit
import GooglePlaces

class PlaceAnnotation: MKPointAnnotation {
    let place: GMSPlace
    init(place: GMSPlace) {
        self.place = place
    }
}

class CustomMapItemAnnotation: MKPointAnnotation {
    var mapItem: MKMapItem?
}
