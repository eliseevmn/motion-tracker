import Foundation

struct GooglePlacesResponse: Codable {
    let results: [GooglePlace]
}

struct GooglePlace: Codable {
    let geometry: LocationPlace
    let name: String
//    let openingHours: OpenNow?
    let photos: [PhotoInfo]
    let types: [String]
    let placeid: String
    let vicinity: String

    enum CodingKeys: String, CodingKey {
        case geometry, name, photos, types, vicinity
        case placeid = "place_id"
    }
}

struct LocationPlace: Codable {

    let location: LatLong

    enum CodingKeys: String, CodingKey {
        case location
    }
}

struct LatLong: Codable {

    let latitude: Double
    let longitude: Double

    enum CodingKeys: String, CodingKey {
        case latitude = "lat"
        case longitude = "lng"
    }
}

struct OpenNow: Codable {

    let isOpen: Bool

    enum CodingKeys: String, CodingKey {
        case isOpen = "open_now"
    }
}

struct PhotoInfo: Codable {

    let height: Int
    let width: Int
    let photoReference: String

    enum CodingKeys: String, CodingKey {
        case height
        case width
        case photoReference = "photo_reference"
    }
}
