import Foundation
import RealmSwift

class Location: Object {
    @objc dynamic var path: String = ""

    convenience init(path: String) {
        self.init()
        self.path = path
    }
}
