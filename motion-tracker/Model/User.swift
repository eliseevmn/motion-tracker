import Foundation
import RealmSwift

class User: Object {
    @objc dynamic var login: String = ""
    @objc dynamic var password: String = ""

    convenience init(login: String, password: String) {
        self.init()
        self.login = login
        self.password = password
    }

    override class func primaryKey() -> String? {
        return "login"
    }
}
