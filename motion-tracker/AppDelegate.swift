import UIKit
import GoogleMaps
import GooglePlaces
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    let notifications = NotificationService()

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        GMSServices.provideAPIKey("AIzaSyC5fU9RmLJ5HroGuxglCtXiFQfp7K1r2Ss")
        GMSPlacesClient.provideAPIKey("AIzaSyC5fU9RmLJ5HroGuxglCtXiFQfp7K1r2Ss")

        notifications.requestAuthorization()
        notifications.scheduleNotification(notififcationType: "Local Notification")
        notifications.notificationCenter.delegate = notifications

        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication,
                     configurationForConnecting connectingSceneSession: UISceneSession,
                     options: UIScene.ConnectionOptions) -> UISceneConfiguration {

        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
}
