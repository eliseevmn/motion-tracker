import UIKit

class BaseCoordinator {

    var childCoordinators: [BaseCoordinator] = []

    func start() {

    }

    func addDependency(_ coordinator: BaseCoordinator) {
        for element in childCoordinators where element === coordinator {
            return
        }
        childCoordinators.append(coordinator)
    }

    func removeDependency(_ coordinator: BaseCoordinator?) {
        guard !childCoordinators.isEmpty,
            let coordinator = coordinator else { return }
        for (index, element) in childCoordinators.reversed().enumerated() where element === coordinator {
            childCoordinators.remove(at: index)
            break
        }
    }

    func setAsRoot(_ controller: UIViewController) {
        let keyWindow: UIWindow? = UIApplication.shared.windows.first(where: { $0.isKeyWindow })
        keyWindow?.rootViewController = controller
    }
}
