import UIKit

class AuthCoordinator: BaseCoordinator {

    var rootController: UINavigationController?
    var onFinishFlow: (() -> Void)?

    override func start() {
        showLoginModule()
    }

    private func showLoginModule() {
        let controller = LoginViewController()

        controller.onRecover = { [weak self] in
            self?.showRecoverModule()
        }

        controller.onLogin = { [weak self] in
            self?.onFinishFlow?()
        }

        controller.onRegistration = { [weak self] in
            self?.showRegistrationModule()
        }

        let rootController = UINavigationController(rootViewController: controller)
        setAsRoot(rootController)
        self.rootController = rootController
    }

    private func showRecoverModule() {
        let controller = RecoveryPasswordViewController()
        rootController?.pushViewController(controller, animated: true)
    }

    private func showRegistrationModule() {
        let controller = RegistrationViewController()
        rootController?.pushViewController(controller, animated: true)
    }
}
