import UIKit

class MainCoordinator: BaseCoordinator {

    var rootController: UINavigationController?
    var onFinishFlow: (() -> Void)?

    override func start() {
        showMainModule()
    }

    private func showMainModule() {
        let controller = MainViewController()

        controller.onMap = { [weak self] in
            self?.showMapModule()
        }

        controller.onLogout = { [weak self] in
            self?.onFinishFlow?()
        }

        let rootController = UINavigationController(rootViewController: controller)
        setAsRoot(rootController)
        self.rootController = rootController
    }

    private func showMapModule() {
        let controller = MenuController()
        rootController?.pushViewController(controller, animated: true)
    }
}
