import UIKit
import RealmSwift
import GoogleMaps

class RealmService {

    static let deleteIfMigration = Realm.Configuration(deleteRealmIfMigrationNeeded: true)

    static func saveRoute(route: GMSMutablePath) {
        let routePath = route.encodedPath()
        let route = Location(path: routePath)
        do {
            let realm = try Realm()
            print(realm.configuration.fileURL ?? "")
            let oldRoute = realm.objects(Location.self)
            try realm.write {
                if oldRoute.count >= 2 {
                    realm.delete(oldRoute.first ?? Location())
                }
                realm.add(route)
            }
        } catch {
            print(error.localizedDescription)
        }
    }

    static func loadPreviuosRoute() -> GMSMutablePath {
        var previousPath = GMSMutablePath()
        do {
            let realm = try Realm()
            let previousRoute = realm.objects(Location.self).first ?? Location()
            previousPath = GMSMutablePath(fromEncodedPath: previousRoute.path) ?? GMSMutablePath()
        } catch {
            print(error.localizedDescription)
        }
        return previousPath
    }

    static func loadLastRoute() -> GMSMutablePath {
        var lastPath = GMSMutablePath()
        do {
            let realm = try Realm()
            let lastRoute = realm.objects(Location.self).last ?? Location()
            lastPath = GMSMutablePath(fromEncodedPath: lastRoute.path) ?? GMSMutablePath()
        } catch {
            print(error.localizedDescription)
        }
        return lastPath
    }

    static func saveUser(user: User) -> Bool {
        do {
            let realm = try Realm()
            print(realm.configuration.fileURL ?? "")
            if let existingUser = realm.object(ofType: User.self, forPrimaryKey: user.login) {
                print(existingUser)
                return false
            }
            try realm.write {
                realm.add(user)
            }
        } catch {
            print(error.localizedDescription)
        }
        return true
    }

    static func signUser(login: String, password: String) -> Bool {
        let regUser = User(login: login, password: password)
        do {
            let realm = try Realm()
            let user = realm.object(ofType: User.self, forPrimaryKey: login)
            if user?.login == regUser.login &&
                user?.password == regUser.password {
                return true
            } else {
                return false
            }
        } catch {
            print(error.localizedDescription)
            return false
        }
    }

    static func recoveryPassword(login: String) -> String {
        do {
            let realm = try Realm()
            let user = realm.object(ofType: User.self, forPrimaryKey: login)
            if user != nil {
                return user?.password ?? "Проблема выгрузки пароля"
            } else {
                return "Неверные данные"
            }
        } catch {
            print(error.localizedDescription)
            return "Неверные данные"
        }
    }
}
