import UIKit
import UserNotifications

class NotificationService: NSObject, UNUserNotificationCenterDelegate {

    let notificationCenter = UNUserNotificationCenter.current()

    // MARK: - Setup notification request
    func requestAuthorization() {
        notificationCenter.requestAuthorization(options: [.alert, .sound, .badge]) { (result, error) in
            if let error = error {
                print("Failed request notifications: ", error.localizedDescription)
                return
            }

            guard result else {
                print("Рарзерение не получено: \(result)")
                return
            }
            self.getNotificationSettings()
        }
    }

    private func getNotificationSettings() {
        notificationCenter.getNotificationSettings { (settings) in
            switch settings.authorizationStatus {
            case .authorized:
                print("Разрешение есть")
            case .denied:
                print("Разрешения нет")
            case .notDetermined:
                print("Неясно, есть или нет разрешение")
            case .provisional:
                print("")
            default:
                fatalError()
            }
        }
    }

    // MARK: - Setup notification content
    func scheduleNotification(notififcationType: String) {
        let content = UNMutableNotificationContent()
        let userAction = "User Action"
        content.title = "Здравствуйте, уважаемый пользователь!"
        content.subtitle = "Вы давно не пользовались приложением"
        content.body = "Не хотите продолжить?"
        content.sound = UNNotificationSound.default
        content.badge = 1
        content.categoryIdentifier = userAction

        guard let path = Bundle.main.path(forResource: "nature", ofType: "png") else {
            print("Не нашел картинку")
            return }

        let url = URL(fileURLWithPath: path)
        do {
            let attachment = try UNNotificationAttachment(identifier: "nature", url: url, options: nil)
            content.attachments = [attachment]
        } catch {
            print("The attachment cold not be loaded")
        }

        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 30, repeats: false)

        let identifire = "Local Notification"
        let request = UNNotificationRequest(identifier: identifire,
                                            content: content,
                                            trigger: trigger)

        notificationCenter.add(request) { (error) in
            if let error = error {
                print("Error \(error.localizedDescription)")
            }
        }

        let snoozeAction = UNNotificationAction(identifier: "Snooze", title: "Отложить", options: [])
        let deleteAction = UNNotificationAction(identifier: "Delete", title: "Удалить", options: [.destructive])

        let category = UNNotificationCategory(identifier: userAction,
                                              actions: [snoozeAction, deleteAction],
                                              intentIdentifiers: [],
                                              options: [])
        notificationCenter.setNotificationCategories([category])
    }

    // MARK: - UNUserNotificationCenterDelegate
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {

        if response.notification.request.identifier == "Local Notification" {
            print("Handling notification with the local Notification Identifire")
        }

        switch response.actionIdentifier {
        case UNNotificationDismissActionIdentifier:
            print("Dismiss Action")
        case UNNotificationDefaultActionIdentifier:
            print("Default")
        case "Snooze":
            print("Snooze")
            scheduleNotification(notififcationType: "Reminder")
        case "Delete":
            print("Delete")
        default:
            print("Unknown actions")
        }

        completionHandler()
    }
}
