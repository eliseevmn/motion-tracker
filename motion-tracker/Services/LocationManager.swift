import Foundation
import CoreLocation
import GoogleMaps
import RxSwift
import RxCocoa

class LocationManager: NSObject {

    // MARK: - Properties
    var locationManager = CLLocationManager()
    var location: BehaviorRelay<CLLocation?> = BehaviorRelay(value: nil)
    var mapView: GMSMapView?

    private var geocoder = CLGeocoder()
    private var marker: GMSMarker?
    private var previuosRoute: GMSPolyline?
    private var previuosPath: GMSMutablePath?
    private var lastPath: GMSMutablePath?
    private var lastRoute: GMSPolyline?

    // MARK: - Init
    required init(location: CLLocation, mapView: GMSMapView) {
        self.location.accept(location)
        self.mapView = mapView
        super.init()
        self.configurateLocationManager()
    }

    // MARK: - Configure location
    private func configurateLocationManager() {
        self.locationManager.requestAlwaysAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.allowsBackgroundLocationUpdates = true
            locationManager.pausesLocationUpdatesAutomatically = false
            locationManager.startMonitoringSignificantLocationChanges()
        }
    }

    // MARK: - Update location
    func startUpdatingLocation() {
        locationManager.startUpdatingLocation()
    }

    func stopUpdatingLocation() {
        locationManager.stopUpdatingLocation()
    }

    func configureCameraMap(coordinate: CLLocationCoordinate2D?) {
        let camera = GMSCameraPosition.camera(withLatitude: coordinate?.latitude ?? 0,
                                              longitude: coordinate?.longitude ?? 0,
                                              zoom: 15)
        mapView?.camera = camera
    }

    private func addMarker(coordinate: CLLocationCoordinate2D, placeMark: CLPlacemark) {
        marker = GMSMarker()
        let customMarker = CustomMarkerGM(
            frame: CGRect(x: 0, y: 0, width: 50, height: 56),
            borderColor: .white)
        marker?.iconView = customMarker
        marker?.position = coordinate
        marker?.infoWindowAnchor = CGPoint(x: 0.5, y: 0)
        marker?.map = mapView
        marker?.zIndex = 5
    }

    private func removeMarker() {
        marker?.map = nil
        marker = nil
    }

    // MARK: - Update Route
    func startRouteTracking() {
        lastRoute?.map = nil
        previuosRoute?.map = nil
        previuosRoute = RoutePolyline(routeWidth: 7, colorRoute: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
        previuosPath = GMSMutablePath()
        previuosRoute?.map = mapView
        startUpdatingLocation()
    }

    func stopRouteTracking() {
        stopUpdatingLocation()

        guard let routePath = self.previuosPath else { return }
        RealmService.saveRoute(route: routePath)
    }

    func loadPreviousRoute() {
        previuosRoute?.map = nil
        previuosRoute = RoutePolyline(routeWidth: 7, colorRoute: #colorLiteral(red: 0.9098039216, green: 0.2705882353, blue: 0.3529411765, alpha: 1))
        previuosPath = RealmService.loadPreviuosRoute()
        previuosRoute?.path = previuosPath
        previuosRoute?.map = mapView

        guard let setBounds = previuosPath else { return }
        let bounds = GMSCoordinateBounds(path: setBounds)
        mapView?.animate(with: GMSCameraUpdate.fit(bounds))
    }

    func loadLastRoute() {
        lastRoute?.map = nil
        lastRoute = RoutePolyline(routeWidth: 7, colorRoute: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1))
        lastPath = RealmService.loadLastRoute()
        lastRoute?.path = lastPath
        lastRoute?.map = mapView
    }
}

// MARK: - CLLocationManagerDelegate
extension LocationManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        removeMarker()

        self.location.accept(locations.last)
        guard let location = self.location.value else { return }

        configureCameraMap(coordinate: location.coordinate)
        geocoder.reverseGeocodeLocation(location) { (places, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            if let place = places?.last {
               self.addMarker(coordinate: location.coordinate, placeMark: place)
            }
        }
        self.previuosPath?.add(location.coordinate)
        self.previuosRoute?.path = self.previuosPath

        self.lastPath?.add(location.coordinate)
        self.lastRoute?.path = self.lastPath
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
}
