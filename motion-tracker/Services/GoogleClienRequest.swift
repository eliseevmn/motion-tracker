import Foundation
import CoreLocation

protocol GoogleClientRequest {

    var googlePlacesKey: String { get set }
    func getGooglePlacesData(
        forKeyword keyword: String,
        location: CLLocation,
        withinMeters radius: Int,
        using completionHandler: @escaping (GooglePlacesResponse) -> Void)
}

class GoogleClient: GoogleClientRequest {

     var googlePlacesKey: String = "AIzaSyC5fU9RmLJ5HroGuxglCtXiFQfp7K1r2Ss"

     func getGooglePlacesData(
        forKeyword keyword: String,
        location: CLLocation,
        withinMeters radius: Int,
        using completionHandler: @escaping (GooglePlacesResponse) -> Void) {

        let session = URLSession(configuration: .default)

        let url = googlePlacesDataURL(forKey: googlePlacesKey,
                                      location: location,
                                      keyword: keyword)

        print(url)
        let task = session.dataTask(with: url) { (data, _, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }

            guard let data = data,
                let response = try? JSONDecoder().decode(GooglePlacesResponse.self, from: data) else {
                    completionHandler(GooglePlacesResponse(results: []))
                    return
            }
            completionHandler(response)
        }
        task.resume()
     }

    private func googlePlacesDataURL(forKey apiKey: String, location: CLLocation, keyword: String) -> URL {

        let baseURL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?"
        let locationString = "location=" + String(location.coordinate.latitude) + "," + String(location.coordinate.longitude)
        let rankby = "rankby=distance"
        let keywrd = "keyword=" + keyword
        let key = "key=" + apiKey
        return URL(string: baseURL + locationString + "&" + rankby + "&" + keywrd + "&" + key)!
    }
}
